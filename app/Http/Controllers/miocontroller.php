<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\newcontact;

class miocontroller extends Controller
{
	public function index(Request $req)
	{
		
		$rubrica= $req->session()->get('rubrica');
        $rubrica = $rubrica ?? [];
        return view('welcome', ['contacts'=>$rubrica, 'count'=>count($rubrica)]);
	}





	public function contactNew(Request $req)
	{
		$name = $req->input('name');
		$surname = $req->input('surname');
		$mobile = $req->input('mobile');
		$email = $req->input('email');

		$contacts=['name'=> $name, 'surname'=> $surname,'mobile'=> $mobile,'email'=> $email   ];
		$req->session()->push('rubrica', $contacts);

		return redirect('/');

	}
	public function contactDetail(Request $req, $id)
	{
		$rubrica= $req->session()->get('rubrica');
		$contact = $rubrica[$id];
		return view('contact', ['contact'=> $contact]);
	}
	public function contactDelete(Request $req, $id){
		$rubrica= $req->session()->get('rubrica');
		array_splice($rubrica, $id , 1 );
		$req->session()->put('rubrica', $rubrica);
		return redirect('/');
	}	
	public function contactOrderBy(Request $req, $field){
		$rubrica= $req->session()->get('rubrica');
		usort($rubrica, function ($item1,$item2) use($field) {
			$dato1 = array_has($item1, $field)  ?  $item1[$field] : false;
			$dato2= array_has($item2,$field)  ?  $item2[$field] : false;
			return $dato1 <=> $dato2;

		});

		$req->session()->put('rubrica', $rubrica);
		return redirect('/');
	}

	public function contactEdit(Request $req, $id){
		 $rubrica= $req->session()->get('rubrica');

        $contact = $rubrica[$id];

        return view('edit', ['contact'=>$contact,
            'id'=>$id]);
	}
public function contactUpdate(Request $req, $id){
		 $name    = $req->input('name');
        $surname = $req->input('surname');
        $mobile  = $req->input('mobile');
        $email  = $req->input('email');

        $contatto = ['name' => $name, 'surname' => $surname, 'mobile' => $mobile, 'email'=>$email];

        $rubrica= $req->session()->get('rubrica');
        $rubrica[$id] = $contatto;
        
        $req->session()->put('rubrica', $rubrica);

        return redirect('/');
	}

	public function form(Request $req){
		$data = $req->except("_token");
		Mail::to($data['email'])->send(new newcontact($data));
		return redirect('/')->with(["status"=>"success",
									"message"=>"grazie del tuo messagio"]);

	}
}
