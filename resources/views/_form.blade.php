<form  method="POST" action="{{ route('contacts.new')}}">
  @csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-4">
      <input name="name" type="text" class="form-control" id="inputName" placeholder="Nome">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputSurname" class="col-sm-4 col-form-label">Cognome</label>
    <div class="col-sm-4">
      <input name="surname" type="text" class="form-control" id="inputSurname" placeholder="Cognome">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputMobilePhone" class="col-sm-4 col-form-label">Numero cellulare</label>
    <div class="col-sm-6">
      <input name="mobile" type="text" class="form-control" id="inputMobilePhone" placeholder="Numero Cellulare">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
    <div class="col-sm-6">
      <input name="email" type="email" class="form-control" id="inputEmail" placeholder="contatto email">
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Aggiungi</button>
</form>