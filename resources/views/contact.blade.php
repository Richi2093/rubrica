@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contatto</div>

                <div class="card-body">
                 <strong>Nome:</strong> {{ $contact['name']}} <br>
                 <strong>Cognome:</strong> {{ $contact['surname']}} <br>
                 <strong>Cellulare:</strong> {{ $contact['mobile']}} <br>
                 @if(array_has($contact,'email'))
                 <strong>Email:</strong> {{ $contact['email']}} <br> @endif
                 <br>
                 <a href="/">Rubrica</a>
                 
                
             </div>
         </div>
     </div>
 </div>
 
</div>
@endsection