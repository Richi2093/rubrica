@extends('layouts.app')

@section('content')
 <form method="POST" action="{{route('contacts.update',[$id])}}">
  @method('PUT')
  @csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-2 col-form-label">Nome</label>
    <div class="col-sm-10">
      <input name="name" value="{{$contact['name']}}" type="text" class="form-control" id="inputName" placeholder="Nome">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputSurname" class="col-sm-2 col-form-label">Cognome</label>
    <div class="col-sm-10">
      <input name="surname" type="text" value="{{$contact['surname']}}" class="form-control" id="inputSurname" placeholder="Cognome">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputMobilePhone" class="col-sm-2 col-form-label">Numero cellulare</label>
    <div class="col-sm-10">
      <input name="mobile" value="{{$contact['mobile']}}" type="text" class="form-control" id="inputMobilePhone" placeholder="Numero Cellulare">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input name="email" value="{{$contact['email']}}" type="email" class="form-control" id="inputEmail" placeholder="contatto email">
    </div>
  </div>
  <button type="submit" class="btn btn-primary">modifica</button>
</form>
@endsection