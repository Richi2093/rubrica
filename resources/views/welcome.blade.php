@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">Aggiungi un nuovo contatto in rubrica</div>

                <div class="card-body">
                    @include('_form')
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rubrica ({{ $count }} contatti)

                    <a href="{{ route( 'contacts.orderby', ['name'])}}">Ordina per nome</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="{{ route( 'contacts.orderby', ['surname'])}}">Ordina per cognome</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="{{ route( 'contacts.orderby', ['email'])}}">Ordina per email</a>
                </div>

                <div class="card-body">
                    @include('_contacts')
                    
                </div>
            </div>
        </div>
    </div>
  <form action="/form" method="POST">
    @csrf
    <div class="form-group">
    <label for="inputName" class="col-sm-2 col-form-label">Nome</label>
   
      <input name="name" type="text" class="form-control" id="inputName" placeholder="Nome">
  </div> 
  <div class="form-group">
    <label for="exampleFormControlInput1">Indirizzo Email</label>
    <input  name="email" type="email" class="form-control" id="exampleFormControlInput1" placeholder="Inserisci Email">
  </div> 
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Messaggio</label>
    <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Invia</button>
</form>
</div>
@endsection