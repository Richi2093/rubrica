<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'miocontroller@index');
Route::post('/contacts/new', 'miocontroller@contactNew')->name('contacts.new');
Route::get('/contacts/{id}', 'miocontroller@contactDetail')
->name('contacts.detail')
->where('id','[0-9]+');
Route::delete('/contacts/{id}', 'miocontroller@contactDelete')
->name('contacts.delete')
->where('id','[0-9]+');
Route::get('/contacts/OrderBy/{field}', 'miocontroller@contactOrderBy')->name('contacts.orderby');
Route::get('/contacts/Edit/{id}' , 'miocontroller@contactEdit')->name('contacts.edit');
Route::put('/contacts/Update/{id}', 'miocontroller@contactUpdate')->name('contacts.update');

Route::post('/form', 'miocontroller@form');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
